export interface IProjectModel {
    id: number;
    name: string;
    description: string;
    userName: string;
    readiness: number;
    userId: string;
}