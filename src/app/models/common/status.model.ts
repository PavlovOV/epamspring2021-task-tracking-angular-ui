export interface IStatusModel {
    id: number;
    name: string;
}