import { IStatusModel } from "src/app/models/common/status.model";

export interface IObjectiveForManagerModel {
    id: number;
    name: string;
    description: string;
 
    status: IStatusModel;

    projectId: number;
    userId: string;
    userName: string;

    minutes: number;
    hours: number;
    totalDays: number;
    durationInString:string;
}


	