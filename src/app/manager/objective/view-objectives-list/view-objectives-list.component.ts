import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { ManagerService } from '../../manager.data.service';
import { IObjectiveForManagerModel } from '../objective.model';

@Component({
  selector: 'app-view-objectives-list',
  templateUrl: './view-objectives-list.component.html',
  styleUrls: ['./view-objectives-list.component.css'],
  providers: [ManagerService, serverErrorHandlerService]
})
export class ViewObjectivesListForManagerComponent implements OnInit {

  public objectives: IObjectiveForManagerModel[] | undefined;
  public projectName: string = "";
  public  projectId: number =0;
  public statusBgColor =['#DFE1E6', '#DEEBFF','#DEEBFF','#E3FCEF'];
  public statusColor =['#42526E', '#0747A6','#0747A6','#006644'];
 
  constructor(private _managerService: ManagerService, private _errorHandler: serverErrorHandlerService,
    private _route: ActivatedRoute,private _router: Router) { }

  ngOnInit(): void {
    this.projectId = Number(this._route.snapshot.paramMap.get('projectId')!);
    this.loadObjectives();
  }

  private loadObjectives() {
        this._managerService.getAllObjectivseFromProject(this.projectId)
      .subscribe(
        x => {
          this.objectives = x;
          for (let objective of this.objectives) {
            objective.durationInString = this.ParseTimeSpanTostring(objective);
          }
        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });

    this._managerService.getProject(this.projectId).
      subscribe(
        x => {
          this.projectName = x.name;
        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });
  }

  private ParseTimeSpanTostring(object: IObjectiveForManagerModel) {
    let str = "";
     if (object.totalDays > 0)
      str = str + object.totalDays + " d "
    if (object.hours > 0)
      str = str + object.hours + " h "
    if (object.minutes > 0)
      str = str + object.minutes + " m"
    return str;
  }

  onCreateObjective() {
    this._router.navigate(["/managerProjects/objective/"+this.projectId+"/create"]);
  }
  
  onSelectObjectives(objectiveId: number) {
    this._router.navigate(["/managerProjects/objectives/viewworklogs/"+objectiveId]);
  }

}
