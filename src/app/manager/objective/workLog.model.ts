export interface IWorkLogModel {
    id: number;
    description: string;
    minutes: number;
    hours: number;
    totalDays: number;
    durationInString:string;
}