import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { ManagerService } from '../../manager.data.service';
import { IDevelopernModel } from '../developer.model';
import { ICreateObjectiveModel} from '../objective.create.model';

@Component({
  selector: 'app-create-objective',
  templateUrl: './create-objective.component.html',
  styleUrls: ['./create-objective.component.css'],
  providers: [ManagerService, serverErrorHandlerService]
})
export class CreateObjectiveComponent implements OnInit {
  errors: string[] = [];
  assigners: IDevelopernModel[] = [];


  myForm: FormGroup = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2)]],
    description: ['', [Validators.required, Validators.minLength(2)]],
    hours: [0, [Validators.required, Validators.min(0)]],
    minutes: [0, [Validators.required, Validators.min(0)]],
    assignerId: [""]
  });

  constructor(private fb: FormBuilder, private _managerService: ManagerService,
    private _errorHandler: serverErrorHandlerService, private _router: Router,
    private _route: ActivatedRoute) { }
  public projectId: number = 0;
  ngOnInit(): void {
    this.projectId = Number(this._route.snapshot.paramMap.get('projectId')!);
    this.loadDevelopers();
  }

  onSubmit(form: FormGroup) {
    let objective: ICreateObjectiveModel =
    {
      id: 0,
      name: form.value.name,
      description: form.value.description,
      minutes: form.value.minutes,
      hours: form.value.hours,
      userId: form.value.assignerId,
      statusId: 0,
      projectId: this.projectId,
    };

    this._managerService.createObjective(objective)
      .subscribe(
        x => {
          this._router.navigate(["/managerProjects/objective", this.projectId]);
        }, err => {
          this.errors = [];
          this._errorHandler.ParseValidationErrors(err, this.errors);
        });
  }

  private loadDevelopers() {
    this._managerService.getDevelopers()
      .subscribe(
        x => {
          this.assigners = x;

        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });
  }
  
  onCancel() {
    this._router.navigate(["/managerProjects/objective", this.projectId]);
  }

}
