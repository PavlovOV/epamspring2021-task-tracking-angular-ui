export interface ICreateObjectiveModel {
    id: number;
    name: string;
    description: string;

    statusId: number;
    projectId: number;
    userId: string;

    hours: number;
    minutes: number;

}
