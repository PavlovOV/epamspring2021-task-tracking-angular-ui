import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IProjectModel } from '../models/common/project.model';
import { IProjectForManagerSaveModel } from './projects/project.save.model';
import { IObjectiveForManagerModel } from './objective/objective.model';
import { IDevelopernModel } from './objective/developer.model';
import { ICreateObjectiveModel } from './objective/objective.create.model';
import { IWorkLogsWithDetailsForManagerModel } from './workLog/worklogs.with.details.for.manager.model';
import { IAssignDeveloperModel } from './workLog/assign.developer.model';


@Injectable()
export class ManagerService {

    private url = environment.apiHost + "/manager";

    constructor(private http: HttpClient) {
    }

    getManagerProjects(managerId: string) {
        return this.http.get<IProjectModel[]>(this.url + "/projects" + '/' + managerId);
    }

    getAllObjectivseFromProject(projectId: number) {
        return this.http.get<IObjectiveForManagerModel[]>(this.url + "/projects/tasks/" + projectId);
    }

    saveProject(project: IProjectForManagerSaveModel) {
        return this.http.put(this.url + "/createproject", project);
    }

    getProject(projectId: number) {
        return this.http.get<IProjectModel>(this.url + "/project" + '/' + projectId);
    }

    getDevelopers() {
        return this.http.get<IDevelopernModel[]>(this.url + "/developers");
    }
    createObjective(objective: ICreateObjectiveModel) {
        return this.http.put(this.url + "/createobjective", objective);
    }

    getWorkLogsForObjective(objectiveId: number) {
        return this.http.get<IWorkLogsWithDetailsForManagerModel>(this.url + "/projects/tasks/logworks/"+objectiveId);
                                                          
    }
    assignDeveloper(model:IAssignDeveloperModel)
    {
        return this.http.post(this.url + "/assigndeveloper", model);
    }

    changeObjectiveStatus(objectiveId:number, statusId:number)
    { 
        return this.http.post(this.url + "/objective/"+objectiveId+"/status/"+statusId, {});
    }

}