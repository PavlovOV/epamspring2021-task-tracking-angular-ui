export interface IAssignDeveloperModel {
    developerId: string;
    objectiveId: number;
}