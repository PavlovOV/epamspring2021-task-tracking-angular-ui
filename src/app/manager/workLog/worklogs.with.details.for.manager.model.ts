import { IStatusModel } from "src/app/models/common/status.model";
import { IObjectiveForManagerModel } from "../objective/objective.model";
import { IWorkLogModel } from "../objective/workLog.model";
import { IProjectModel } from "../../models/common/project.model";

export interface IWorkLogsWithDetailsForManagerModel {
    project :IProjectModel;
    objective: IObjectiveForManagerModel;
    description: string;

    workLogs: IWorkLogModel[];
    possibleStatuses: IStatusModel[];

    minutes: number;
    hours: number;
    totalDays: number;
    durationInString:string;
}
