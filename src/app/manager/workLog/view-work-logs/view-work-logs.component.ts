import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { ManagerService } from '../../manager.data.service';
import { FormBuilder, FormGroup } from '@angular/forms'
import { IWorkLogsWithDetailsForManagerModel } from '../worklogs.with.details.for.manager.model';
import { IStatusModel } from 'src/app/models/common/status.model';
import { IDevelopernModel } from '../../objective/developer.model';
import { IAssignDeveloperModel } from '../assign.developer.model';

@Component({
  selector: 'app-view-work-logs',
  templateUrl: './view-work-logs.component.html',
  styleUrls: ['./view-work-logs.component.css'],
  providers: [ManagerService, serverErrorHandlerService]
})
export class ViewWorkLogsForManagerComponent implements OnInit {

  statusForm: FormGroup = this.fb.group({
    statusId: [1]
  });

  assignDeveloperForm: FormGroup = this.fb.group({
    assignerId: [""]
  });

  public statuses: IStatusModel[] = [];
  public assigners: IDevelopernModel[] = [];

  workLogId: number = 0;
  public worklLogsWithDetails: IWorkLogsWithDetailsForManagerModel | undefined;
  constructor(private fb: FormBuilder, private _managerService: ManagerService,
    private _errorHandler: serverErrorHandlerService, private _route: ActivatedRoute) { }


  ngOnInit(): void {
    this.workLogId = Number(this._route.snapshot.paramMap.get('objectiveId')!);
    this.loadWorkLogs();
  }

  private loadDevelopers() {

    this._managerService.getDevelopers()
      .subscribe(
        x => {
          this.assigners = x;

        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });
  }

  private loadWorkLogs() {

    this._managerService.getWorkLogsForObjective(this.workLogId)
      .subscribe(
        x => {
          this.worklLogsWithDetails = x;
          this.statuses = x.possibleStatuses;

          let firstStatus = 0;
          if (this.statuses.length > 0)
            firstStatus = this.statuses[0].id;
          this.statusForm.controls['statusId'].setValue(firstStatus, { onlySelf: true });

          for (let workLog of this.worklLogsWithDetails.workLogs)
            workLog.durationInString = this.ParseTimeSpanTostring(workLog);

          this.worklLogsWithDetails.objective.durationInString = this.ParseTimeSpanTostring(this.worklLogsWithDetails.objective);

          this.worklLogsWithDetails.durationInString = this.ParseTimeSpanTostring(this.worklLogsWithDetails);
          if (!this.IsDeveloperAssigned())
            this.loadDevelopers()
        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });
  }

  IsDeveloperAssigned() {
    return !(this.worklLogsWithDetails!.objective.userId == null);
  }

  CanChangeStatus() {
    return this.IsDeveloperAssigned() && this.statuses.length > 0;
  }

  private ParseTimeSpanTostring(object: any) {
    let str = "";
    if (object.totalDays > 0)
      str = str + object.totalDays + " d "
    if (object.hours > 0)
      str = str + object.hours + " h "
    if (object.minutes > 0)
      str = str + object.minutes + " m"
    return str;
  }

  onStatusSubmit(form: FormGroup) {

    this._managerService.changeObjectiveStatus(this.worklLogsWithDetails!.objective.id,form.value.statusId)
    .subscribe(
      x => {
        this.loadWorkLogs();
       }, err => {
        
        this._errorHandler.RedirectToErrors(err);
      });
    

  }

  onAssignDeveloperSubmit(form: FormGroup) {

    let assignDeveloper: IAssignDeveloperModel =
    {
      developerId: form.value.assignerId,
      objectiveId: Number( this.worklLogsWithDetails!.objective.id),
    };

    this._managerService.assignDeveloper(assignDeveloper)
      .subscribe(
        x => {
          this.loadWorkLogs();
         }, err => {
          
          this._errorHandler.RedirectToErrors(err);
        });

  }
}
