import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { ManagerService } from '../../manager.data.service';
import { IProjectModel } from '../../../models/common/project.model';
import { LocalStorageService } from 'src/app/_helpers/local-storage.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  providers: [ManagerService, serverErrorHandlerService]
})
export class ProjectsForManagerComponent implements OnInit {

  public projects: IProjectModel[] | undefined;
  constructor(private _managerService: ManagerService, private _errorHandler: serverErrorHandlerService,
    private _router: Router, private _storageService: LocalStorageService) { }

  ngOnInit(): void {

    this.loadProjects();
    var x = 150;
    console.log(x.toFixed(2) + "%");
  }

  loadProjects() {
    let id = this._storageService.loadUserId(); 
    this._managerService.getManagerProjects(id!)
      .subscribe(
        x => {
          this.projects = x;
        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });
  }

  onSelectProject(projectId: number) {
    this._router.navigate(["/managerProjects/objective", projectId]);
  }

  onCreateProject() {
    this._router.navigate(["/managerProjects/create"]);
  }
}
