
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { ManagerService } from '../../manager.data.service';
import { IProjectForManagerSaveModel } from '../project.save.model';


@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css'],
  providers: [ManagerService, serverErrorHandlerService]
})
export class CreateProjectComponent implements OnInit {
  errors: string[] = [];
  myForm: FormGroup = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2)]],
    description: ['', [Validators.required, Validators.minLength(2)]]
  });
  constructor(private fb: FormBuilder, private _router: Router,
    private _managerService: ManagerService, private _errorHandler: serverErrorHandlerService) { }
  ngOnInit() {

  }

  onSubmit(form: FormGroup) {

    let project: IProjectForManagerSaveModel = {
      name: form.value.name,
      description: form.value.description,
    };

    this._managerService.saveProject(project)
      .subscribe(
        x => {
          this._router.navigate(["/managerProjects"]);
        }, err => {
          this.errors= [];
          this._errorHandler.ParseValidationErrors(err,this.errors );
      });
  }
  
  onCancel() {
    this._router.navigate(["/managerProjects"]);
  }
}