import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { PagingInfo } from "./paganing-info";

@Component({
    selector: 'paganing',
    templateUrl: './paganing.component.html',
    styleUrls: ['./paganing.component.css'],

})
export class PaganingComponent {

    @Input() pageInfo: PagingInfo = new PagingInfo(0, 0, 0,0);
    @Output() PageChanged = new EventEmitter<number>();
    pages: number[] = [];


    onClick(p: number) {
        this.pageInfo!.currentPage = p;
        this.PageChanged.emit(p);
    }
    ngOnChanges() {
        this.pages = [];
        for (var i = 0; i < this.pageInfo.totalPages; i++)
            this.pages.push(i + 1);
     }


}
