import { Injectable } from "@angular/core";
import { ILoginResponseModel } from "../account/login-response.model";

@Injectable()
export class LocalStorageService {

    saveLoginResponse(loginResponse: ILoginResponseModel) {
        localStorage.setItem("jwt", loginResponse.token);
        localStorage.setItem("userName", loginResponse.userName);
        localStorage.setItem("userId", loginResponse.userId);
        localStorage.setItem("userRoles", JSON.stringify(loginResponse.roles));
    }

    loadLoginResponse(): ILoginResponseModel {
        let model: ILoginResponseModel = {
            roles: [],
            userName: localStorage.getItem("userName")!,
            token: localStorage.getItem("jwt")!,
            userId: localStorage.getItem("userId")!
        }

        let item = localStorage.getItem("userRoles");
        if (item)
            model.roles = JSON.parse(item);
        return model;
    }
    
    loadUserId(): string {
        return localStorage.getItem("userId")!;
    }

    removeData()
    {
        localStorage.removeItem("jwt");
        localStorage.removeItem("userName");
        localStorage.removeItem("userRole");
        localStorage.removeItem("uuserId");
    }
}