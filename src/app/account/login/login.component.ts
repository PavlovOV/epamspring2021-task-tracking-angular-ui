import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ILoginResponseModel } from '../login-response.model';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { CrossRoadsComponent } from '../cross-roads/cross-roads.service';
import { ILoginModel } from '../login.model';
import { AuthService } from '../auth.service';
import { LocalStorageService } from 'src/app/_helpers/local-storage.service';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [serverErrorHandlerService, CrossRoadsComponent, AuthService, LocalStorageService]
})
export class LoginComponent implements OnInit {

  errors: string[] = [];
  myForm: FormGroup = this.fb.group({
    username: ['', [Validators.required, Validators.minLength(2)]],
    password: ['', [Validators.required, Validators.minLength(2)]],
  });


  constructor(private fb: FormBuilder,
    private errorHandler: serverErrorHandlerService, private crossRoads: CrossRoadsComponent,
    private _authService: AuthService, private _storageService: LocalStorageService) { }

  ngOnInit() { }

  login(form: FormGroup) {
    this.errors = [];

    let model: ILoginModel = {
      name: form.value.username,
      password: form.value.password
    }

    this._authService.login(model).subscribe(response => {
      this._storageService.saveLoginResponse(<ILoginResponseModel>response);
      this.crossRoads.selectRoad();
    }, err => {
      this.errors = [];
      this.errorHandler.ParseValidationErrors(err, this.errors);
    }
    );
  }

}
