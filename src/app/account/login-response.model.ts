export interface ILoginResponseModel {
    token: string;
    userName: string;
    roles: string[];
    userId: string;
}
