import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { SubscriptionLike } from 'rxjs';
import { ILoginResponseModel } from '../login-response.model';
import { CrossRoadsComponent } from '../cross-roads/cross-roads.service';
import { IRegisterModel } from '../register.model';
import { AuthService } from '../auth.service';
import { LocalStorageService } from 'src/app/_helpers/local-storage.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [serverErrorHandlerService, CrossRoadsComponent, LocalStorageService, AuthService]

})
export class RegisterComponent implements OnInit {

  errors: string[] = [];
  private subscriptions: SubscriptionLike | undefined;

  myForm: FormGroup = this.fb.group({
    username: ['', [Validators.required, Validators.minLength(2)]],
    password: ['', [Validators.required, Validators.minLength(2)]],
    confirmPassword: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
  },
    { validator: this.MustMatch('password', 'confirmPassword') }
  );

  constructor(private fb: FormBuilder,
    private _errorHandler: serverErrorHandlerService, private _crossRoads: CrossRoadsComponent,
    private _authService: AuthService, private _storageService: LocalStorageService) {
  }
  ngOnInit(): void {
  }

  register(form: FormGroup) {

    let model: IRegisterModel = {
      name: form.value.username,
      password: form.value.password,
      email: form.value.email
    }

    this.subscriptions = this._authService.register(model).subscribe(response => {

      if (this.subscriptions) {
        this.subscriptions.unsubscribe();
      }
      this._storageService.saveLoginResponse(<ILoginResponseModel>response);
      this._crossRoads.selectRoad();
    }, err => {
      this.errors = [];
      this._errorHandler.ParseValidationErrors(err, this.errors);
    });
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

}
