export interface IRegisterModel {
    password: string;
    name: string;
    email: string;
}