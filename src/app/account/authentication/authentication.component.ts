import { Component, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/modals/confirm-dialog/confirm-dialog.component';

import { MatDialog } from '@angular/material/dialog';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/_helpers/local-storage.service';
import { ILoginResponseModel } from '../login-response.model';
@Component({
  selector: 'authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {
  userName: string = "";
  constructor(private jwtHelper: JwtHelperService, private router: Router, private dialog: MatDialog,
    private _storageService: LocalStorageService) { }

  ngOnInit(): void {
  }

  IsAuthenticated() {
    let userInfo: ILoginResponseModel = this._storageService.loadLoginResponse();
    this.userName = userInfo.userName;
    if (userInfo.token && !this.jwtHelper.isTokenExpired(userInfo.token)) {
      return true;
    }
    return false;
  }

  logOut() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: {
        title: "Are you sure?",
        message: "You are about to log out "
      }
    });

    dialogRef.afterClosed().subscribe(dialogResult => {

      console.log(dialogResult);
      if (dialogResult) {
        this._storageService.removeData();
        this.router.navigate(["/login"]);
      }
    });

  }
}
