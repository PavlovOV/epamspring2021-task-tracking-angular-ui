import {  Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoleViewerService } from '../guards/role.viewer.service';



@Injectable()
export class CrossRoadsComponent implements OnInit {

  constructor(private _roleViewerService: RoleViewerService, private _router: Router) { }

  ngOnInit(): void {
    this.selectRoad();
  }

  selectRoad() {
    if (this._roleViewerService.IsAdmin()) {
      this._router.navigate(["/roles_and_users"]);
      return;
    }
    if (this._roleViewerService.IsManager()) {
      this._router.navigate(["/managerProjects"]);
      return;
    }
    if (this._roleViewerService.IsDeveloper()) {
      this._router.navigate(["/developerProjects"]);
      return;
    }
    this._router.navigate(["/login"]);
    
  }
}
