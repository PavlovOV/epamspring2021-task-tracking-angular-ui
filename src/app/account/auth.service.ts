import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ILoginResponseModel } from './login-response.model';
import { ILoginModel } from './login.model';
import { IRegisterModel } from './register.model';


@Injectable()
export class AuthService {

    private url = environment.apiHost + "/account";

    constructor(private http: HttpClient) {
    }

    login(model: ILoginModel) {
        return this.http.post<ILoginResponseModel>(this.url + '/login', model)
    }
    register(model: IRegisterModel) {
        return this.http.put<ILoginResponseModel>(this.url + '/register', model)
    }
}