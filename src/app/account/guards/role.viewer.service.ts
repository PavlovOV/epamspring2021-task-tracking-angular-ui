import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LocalStorageService } from 'src/app/_helpers/local-storage.service';
import { ILoginResponseModel } from '../login-response.model';

@Injectable()
export class RoleViewerService {
    loginResponse: ILoginResponseModel = { token: "", userName: "", roles: [] ,userId:"" };

    constructor(private jwtHelper: JwtHelperService, private _storageService: LocalStorageService) {
    }

    IsAuthenticated() {
       
        this.loginResponse= this._storageService.loadLoginResponse();
        if (this.loginResponse.token && !this.jwtHelper.isTokenExpired(this.loginResponse.token))
            return true;
        return false;
    }

    IsAdmin() {
        if (this.IsAuthenticated())
            if (this.loginResponse.roles.includes("Administrator"))
                return true;
        return false;
    }

    IsManager() {
        if (this.IsAuthenticated())
            if (this.loginResponse.roles.includes("Manager"))
                return true;
        return false;
    }
    IsDeveloper() {
        if (this.IsAuthenticated())
            if (this.loginResponse.roles.includes("Developer"))
                return true;
        return false;
    }
}