import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ILoginResponseModel } from '../login-response.model';
import { RoleViewerService } from './role.viewer.service';

@Injectable()
export class ManagerGuardService implements CanActivate {
    loginResponse: ILoginResponseModel = { token: "", userName: "", roles: [], userId: "" };

    constructor( private router: Router,
        private _roleViewerService: RoleViewerService) {
    }

    canActivate() {
        if (this._roleViewerService.IsManager())
            return true;
        this.router.navigate(["/page_forbidden", { error: "Login as Manager" }]);
        return false;
    }
}