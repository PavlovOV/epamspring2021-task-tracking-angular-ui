import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { DeveloperService } from '../../developer.data.service';
import { IObjectiveForDeveloperModel } from '../objective.model';



@Component({
  selector: 'app-view-objectives-list',
  templateUrl: './view-objectives-list.component.html',
  styleUrls: ['./view-objectives-list.component.css'],
  providers: [DeveloperService, serverErrorHandlerService]
})
export class ViewObjectivesListForDeveloperComponent implements OnInit {

  public objectives: IObjectiveForDeveloperModel[] | undefined;
  public projectName: string = "";
  public  projectId: number =0;
  public statusBgColor =['#DFE1E6', '#DEEBFF','#DEEBFF','#E3FCEF'];
  public statusColor =['#42526E', '#0747A6','#0747A6','#006644'];
 
  constructor(private _developerService: DeveloperService, private _errorHandler: serverErrorHandlerService,
    private _route: ActivatedRoute,private _router: Router) { }

  ngOnInit(): void {
    this.projectId = Number(this._route.snapshot.paramMap.get('projectId')!);
    this.loadObjectives();
  }

  private loadObjectives() {
        this._developerService.getObjectivseFromProject(this.projectId)
      .subscribe(
        x => {
          this.objectives = x;
          for (let objective of this.objectives) {
            objective.durationInString = this.ParseTimeSpanTostring(objective);
          }
        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });

    this._developerService.getProject(this.projectId).
      subscribe(
        x => {
          this.projectName = x.name;
        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });
  }

  private ParseTimeSpanTostring(object: IObjectiveForDeveloperModel) {
    let str = "";
     if (object.totalDays > 0)
      str = str + object.totalDays + " d "
    if (object.hours > 0)
      str = str + object.hours + " h "
    if (object.minutes > 0)
      str = str + object.minutes + " m"
    return str;
  }

  
  onSelectObjectives(objectiveId: number) {
    this._router.navigate(["/developerProjects/objectives/viewworklogs/"+objectiveId]);
  }

}
