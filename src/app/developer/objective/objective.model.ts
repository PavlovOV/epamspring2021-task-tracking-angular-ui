import { IStatusModel } from "src/app/models/common/status.model";

export interface IObjectiveForDeveloperModel {
    id: number;
    name: string;
    description: string;
 
    status: IStatusModel;

    projectId: number;
 
    minutes: number;
    hours: number;
    totalDays: number;
    durationInString:string;
}