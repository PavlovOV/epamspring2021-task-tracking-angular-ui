import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IProjectModel } from '../models/common/project.model';
import { IObjectiveForDeveloperModel } from './objective/objective.model';
import { IWorkLogsWithDetailsForDeveloperModel } from './workLog/worklogs.with.details.for.developer.model';
import { ICreateWorkLogModel } from './workLog/worklog-create.model';



@Injectable()
export class DeveloperService {

    private url = environment.apiHost + "/developer";

    constructor(private http: HttpClient) {
    }

    getProject(projectId: number) {
        return this.http.get<IProjectModel>(this.url + "/project" + '/' + projectId);
    }
    
    getDeveloperProjects(developerId: string) {
        return this.http.get<IProjectModel[]>(this.url + "/Projects" + '/' + developerId);
    }

    getObjectivseFromProject(projectId: number) {
        return this.http.get<IObjectiveForDeveloperModel[]>(this.url + "/projects/tasks/" + projectId);
    }

    getWorkLogsForObjective(objectiveId: number) {
        return this.http.get<IWorkLogsWithDetailsForDeveloperModel>(this.url + "/projects/tasks/logworks/"+objectiveId);                                                   
    }
    
    changeObjectiveStatus(objectiveId:number, statusId:number)
    { 
        return this.http.post(this.url + "/objective/"+objectiveId+"/status/"+statusId, {});
    }
    createObjective(worklog: ICreateWorkLogModel) {
        return this.http.put(this.url + "/createworklog", worklog);
    }

}