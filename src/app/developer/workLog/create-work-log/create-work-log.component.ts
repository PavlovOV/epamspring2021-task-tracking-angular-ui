import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { DeveloperService } from '../../developer.data.service';
import { ICreateWorkLogModel } from '../worklog-create.model';

@Component({
  selector: 'app-create-work-log',
  templateUrl: './create-work-log.component.html',
  styleUrls: ['./create-work-log.component.css'],
  providers: [DeveloperService, serverErrorHandlerService]
})
export class CreateWorkLogComponent implements OnInit {

  errors: string[] = [];
  
  objectiveId: number = 0;
  myForm: FormGroup = this.fb.group({
    hours: [0, [Validators.required, Validators.min(0)]],
    minutes: [0, [Validators.required, Validators.min(0)]],
    description: ['', [Validators.required, Validators.minLength(2)]]
  });
  constructor(private fb: FormBuilder, private _router: Router,
    private _developerService: DeveloperService, private _errorHandler: serverErrorHandlerService,
   private _route: ActivatedRoute) { }
  ngOnInit() {
    this.objectiveId = Number(this._route.snapshot.paramMap.get('objectiveId')!);
  }

  onSubmit(form: FormGroup) {

    let workLog: ICreateWorkLogModel = {
      description: form.value.description,
      minutes: form.value.minutes,
      hours: form.value.hours,
      objectiveId: this.objectiveId,
    };

    this._developerService.createObjective(workLog)
      .subscribe(
        x => {
          this._router.navigate(["/developerProjects/objectives/viewworklogs/"+this.objectiveId]);
        }, err => {
          this.errors= [];
          this._errorHandler.ParseValidationErrors(err,this.errors );
      });
  }
  
  onCancel() {
    this._router.navigate(["/developerProjects/objectives/viewworklogs/"+this.objectiveId]);
  }

}
