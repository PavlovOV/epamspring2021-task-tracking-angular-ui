export interface ICreateWorkLogModel {
    description: string;
   
    objectiveId: number;

    hours: number;
    minutes: number;

}