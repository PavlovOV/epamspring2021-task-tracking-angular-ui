import { IStatusModel } from "src/app/models/common/status.model";
import { IObjectiveForDeveloperModel } from "../objective/objective.model";
import { IWorkLogModel } from "../../manager/objective/workLog.model";
import { IProjectModel } from "../../models/common/project.model";

export interface IWorkLogsWithDetailsForDeveloperModel {
    project :IProjectModel;
    objective: IObjectiveForDeveloperModel;
    description: string;

    workLogs: IWorkLogModel[];
    possibleStatuses: IStatusModel[];

    minutes: number;
    hours: number;
    totalDays: number;
    durationInString:string;
    isAllowedToAddLogWork:boolean;
}
