import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { DeveloperService } from '../../developer.data.service';
import { FormBuilder, FormGroup } from '@angular/forms'
import { IWorkLogsWithDetailsForDeveloperModel } from '../worklogs.with.details.for.developer.model';
import { IStatusModel } from 'src/app/models/common/status.model';


@Component({
  selector: 'app-view-work-logs',
  templateUrl: './view-work-logs.component.html',
  styleUrls: ['./view-work-logs.component.css'],
  providers: [DeveloperService, serverErrorHandlerService]
})
export class ViewWorkLogsForDeveloperComponent implements OnInit {

  statusForm: FormGroup = this.fb.group({
    statusId: [1]
  });

  public statuses: IStatusModel[] = [];
  workLogId: number = 0;
  public worklLogsWithDetails: IWorkLogsWithDetailsForDeveloperModel | undefined;
  
  public CanChangeStatus:boolean| undefined;
  public isAllowedToAddLogWork:boolean| undefined;

  constructor(private fb: FormBuilder, private _developerService: DeveloperService,
    private _errorHandler: serverErrorHandlerService, private _route: ActivatedRoute,
    private _router: Router) { }


  ngOnInit(): void {
    this.workLogId = Number(this._route.snapshot.paramMap.get('objectiveId')!);
    this.loadWorkLogs();
  }

  private loadWorkLogs() {

    this._developerService.getWorkLogsForObjective(this.workLogId)
      .subscribe(
        x => {
          this.worklLogsWithDetails = x;
          this.statuses = x.possibleStatuses;
          let firstStatus = 0;
          if (this.statuses.length > 0)
            firstStatus = this.statuses[0].id;
          this.statusForm.controls['statusId'].setValue(firstStatus, { onlySelf: true });

          for (let workLog of this.worklLogsWithDetails.workLogs)
            workLog.durationInString = this.ParseTimeSpanTostring(workLog);

          this.worklLogsWithDetails.objective.durationInString = this.ParseTimeSpanTostring(this.worklLogsWithDetails.objective);

          this.worklLogsWithDetails.durationInString = this.ParseTimeSpanTostring(this.worklLogsWithDetails);

         this.setBooleanFieldsForTemplateDraw();

        }, err => {
          this._errorHandler.RedirectToErrors(err);
        });
  }


  private ParseTimeSpanTostring(object: any) {
    let str = "";
    if (object.totalDays > 0)
      str = str + object.totalDays + " d "
    if (object.hours > 0)
      str = str + object.hours + " h "
    if (object.minutes > 0)
      str = str + object.minutes + " m"
    return str;
  }

  onStatusSubmit(form: FormGroup) {

    this._developerService.changeObjectiveStatus(this.worklLogsWithDetails!.objective.id, form.value.statusId)
      .subscribe(
        x => {
          this.loadWorkLogs();
        }, err => {

          this._errorHandler.RedirectToErrors(err);
        });


  }

  
  onAddWorkLog()
  {
    this._router.navigate(["/developerProjects/objectives/viewworklogs/create",this.worklLogsWithDetails!.objective.id]);
  }

  private setBooleanFieldsForTemplateDraw() {

    this.CanChangeStatus = (this.statuses.length > 0);
    this.isAllowedToAddLogWork = this.worklLogsWithDetails!.isAllowedToAddLogWork;
  }
}
