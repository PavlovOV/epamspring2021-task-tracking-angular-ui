import { PagingInfo } from "src/app/_helpers/paganing/paganing-info";
import { IUserForAdminModel } from "./user-for-admin-model";

export interface  IUsersForAdminModel
{
    users :IUserForAdminModel[];
    pagingInfo: PagingInfo;
}