import { SortingOrder } from "src/app/_helpers/sorting-order";

export class UserSortFilterForAdminData {
    constructor(
        public Name: string,
        public NameOrder: SortingOrder,
        public CurrentPage: number,
        public RoleId: string
    ) { }
}