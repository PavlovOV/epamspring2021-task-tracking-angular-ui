import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { SortingOrder } from "src/app/_helpers/sorting-order";
import { UserSortFilterForAdminData } from "./UserSearchFilterForAdminData";

@Component({
  selector: 'user-sort-filter-form',
  templateUrl: './user-sort-filter-form.component.html',
  styleUrls: ['./user-sort-filter-form.component.css']
})
export class UserSortFilterFormForAdminComponent implements OnInit {

  @Output() SortFilterDataToSend = new EventEmitter<UserSortFilterForAdminData>();

  searchFilter = new FormGroup({
    name: new FormControl(''),
  });

  
  nameOrder = { value: SortingOrder.ascending };

  arrowUpCode = '&#8593';
  arrowDownCode = '&#8595';
  arrowUpAndDownCode = '&#8597';

  nameArrow = { value: "" };

  constructor() { }

  ngOnInit(): void {
    this.applyOrderOnView(this.nameOrder, this.nameArrow);
  }

  onSubmit(form: FormGroup) {
    let searchFilterData = new UserSortFilterForAdminData(form.value.name, this.nameOrder.value, 0, "");
    this.SortFilterDataToSend.emit(searchFilterData);
  }

  onNameClick() {
    this.ReverseOrder(this.nameOrder);
    this.applyOrderOnView(this.nameOrder, this.nameArrow);

    let searchFilterData = new UserSortFilterForAdminData(this.searchFilter.value.name, this.nameOrder.value, 0, "");
    this.SortFilterDataToSend.emit(searchFilterData);
  }

  applyOrderOnView(order: any, arrow: any) {
    switch (order.value) {
      case SortingOrder.unOrder:
        arrow.value = this.arrowUpAndDownCode;
        break;
      case SortingOrder.ascending:
        arrow.value = this.arrowDownCode;
        break;
      case SortingOrder.descending:
        arrow.value = this.arrowUpCode;
        break;
    }
  }

  ReverseOrder(element: any,) {
    switch (element.value) {
      case SortingOrder.unOrder:
        element.value = SortingOrder.ascending;
        break;
      case SortingOrder.ascending:
        element.value = SortingOrder.descending;
        break;
      case SortingOrder.descending:
        element.value = SortingOrder.ascending;
        break;
    }

  }

}
