export interface IUserForAdminModel {
    id: string;
    name: string;
    email: string;
}