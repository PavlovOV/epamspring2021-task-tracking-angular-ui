import { Component, OnInit } from '@angular/core';
import { SubscriptionLike } from 'rxjs';
import { serverErrorHandlerService } from 'src/app/error.handler/server-error.handler.service';
import { PagingInfo } from 'src/app/_helpers/paganing/paganing-info';
import { SortingOrder } from 'src/app/_helpers/sorting-order';
import { AdminService } from '../admin.data.service';
import { IRoleModel } from '../role.model';
import { IUserForAdminModel } from './user-for-admin-model';
import { UserSortFilterForAdminData } from './user-sort-filter-form/UserSearchFilterForAdminData';

@Component({
  selector: 'app-roles-and-users',
  templateUrl: './roles-and-users.component.html',
  styleUrls: ['./roles-and-users.component.css'],
  providers: [AdminService, serverErrorHandlerService]
})
export class RolesAndUsersComponent implements OnInit {

  public roles: IRoleModel[] | undefined;
  public users: IUserForAdminModel[] | undefined;
  public selectedId: string = "";
  pageInfo: PagingInfo = new PagingInfo(0, 0, 0,0);

  dataForFilterSort: UserSortFilterForAdminData = new UserSortFilterForAdminData("", SortingOrder.ascending, 1, "");
  private subscriptions_r: SubscriptionLike | undefined;

  constructor(private _adminService: AdminService, private errorHandler: serverErrorHandlerService) { }

  ngOnInit(): void {

    this.subscriptions_r =
      this._adminService.getRoles().subscribe(
        x => this.roles = x,

        error => this.errorHandler.RedirectToErrors(error)
      );
    this.loadUsers();
  }

  loadUsers() {

    this._adminService.getUsers(this.dataForFilterSort)
      .subscribe(
        x => {
          this.users = x.users;
          this.pageInfo = x.pagingInfo;

        }, err => {
          this.errorHandler.RedirectToErrors(err);

        });
  }

  OnGetDataforSortFilter(data: UserSortFilterForAdminData) {
    this.dataForFilterSort = data;

    this.ResetPage();
    this.loadUsers();
  }

  OnPageChanged(page: number) {
     this.dataForFilterSort.CurrentPage = page;
    this.loadUsers();
  }

  ResetPage() {
     this.dataForFilterSort.CurrentPage = 1;
  }
  onRoleSelect(id: string) {
    this.selectedId = id;
    this.dataForFilterSort.RoleId = id;
    this.ResetPage();
    this.loadUsers();
  }

  onSelectUser(id: string) {
    //todo
  }

  isSelected(id: string) {
    return id === this.selectedId;
  }

  ngOnDestroy() {
    if (this.subscriptions_r)
      this.subscriptions_r.unsubscribe();
  }
}
