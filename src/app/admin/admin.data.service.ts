import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IRoleModel } from './role.model';
import { UserSortFilterForAdminData } from './roles-and-users/user-sort-filter-form/UserSearchFilterForAdminData';
import { IUsersForAdminModel } from './roles-and-users/users-for-admin-model';


@Injectable()
export class AdminService {

    private url = environment.apiHost + "/admin";

    constructor(private http: HttpClient) {
        console.log(" on init DataService");
    }

    getRoles() {
        console.log(this.url);

        return this.http.get<IRoleModel[]>(this.url + "/roles");
    }

    getUsers(filter: UserSortFilterForAdminData) {
        console.log(this.url);
        return this.http.post<IUsersForAdminModel>(this.url + "/users", filter);
    }

}