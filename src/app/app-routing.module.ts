import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EdgingComponent } from './edging/edging.component';
import { LoginComponent } from './account/login/login.component';
import { RegisterComponent } from './account/register/register.component';
import { PageForbiddenComponent } from './error.handler/page-forbidden/page-forbidden.component';
import { PageNotFoundComponent } from './error.handler/page-not-found/page-not-found.component';
import { RolesAndUsersComponent } from './admin/roles-and-users/roles-and-users.component';
import { ProjectsForManagerComponent } from './manager/projects/view-projects-list/projects.component';
import { CreateProjectComponent } from './manager/projects/create-project/create-project.component';
import { ViewObjectivesListForManagerComponent } from './manager/objective/view-objectives-list/view-objectives-list.component';

import { CreateObjectiveComponent } from './manager/objective/create-objective/create-objective.component';
import { ViewWorkLogsForManagerComponent } from './manager/workLog/view-work-logs/view-work-logs.component';
import { ProjectsForDeveloperComponent } from './developer/projects/view-projects-list/projects.component';
import { ViewObjectivesListForDeveloperComponent } from './developer/objective/view-objectives-list/view-objectives-list.component';
import { ViewWorkLogsForDeveloperComponent } from './developer/workLog/view-work-logs/view-work-logs.component'
import { CreateWorkLogComponent } from './developer/workLog/create-work-log/create-work-log.component';
import { CrossRoadsComponent } from './account/cross-roads/cross-roads.service';
import { DeveloperGuardService } from './account/guards/developer-guard.service';
import { ManagerGuardService } from './account/guards/manager-guard.service';
import { AdminGuardService } from './account/guards/admin-guard.service';

const routes: Routes = [
  { path: "", redirectTo: "/crossroads", pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  //{ path: 'crossroads', component: CrossRoadsComponent },
  { path: 'page_forbidden/:error', component: PageForbiddenComponent },
  { path: 'page_notfound/:error', component: PageNotFoundComponent },
  { path: 'roles_and_users', component: RolesAndUsersComponent, canActivate: [AdminGuardService] },
  { path: 'managerProjects', component: ProjectsForManagerComponent, canActivate: [ManagerGuardService] },
  {
    path: 'managerProjects/objective/:projectId', component: ViewObjectivesListForManagerComponent,
    canActivate: [ManagerGuardService]
  },
  {
    path: 'managerProjects/objective/:projectId/create', component: CreateObjectiveComponent,
    canActivate: [ManagerGuardService]
  },
  { path: 'managerProjects/create', component: CreateProjectComponent, canActivate: [ManagerGuardService] },
  {
    path: 'managerProjects/objectives/viewworklogs/:objectiveId', component: ViewWorkLogsForManagerComponent,
    canActivate: [ManagerGuardService]
  },
  { path: 'developerProjects', component: ProjectsForDeveloperComponent, canActivate: [DeveloperGuardService] },
  {
    path: 'developerProjects/objective/:projectId', component: ViewObjectivesListForDeveloperComponent,
    canActivate: [DeveloperGuardService]
  },
  {
    path: 'developerProjects/objectives/viewworklogs/:objectiveId', component: ViewWorkLogsForDeveloperComponent,
    canActivate: [DeveloperGuardService]
  },
  {
    path: 'developerProjects/objectives/viewworklogs/create/:objectiveId', component: CreateWorkLogComponent,
    canActivate: [DeveloperGuardService]
  },
  { path: "**", component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [
  EdgingComponent,
  LoginComponent,
  RegisterComponent,
  //CrossRoadsComponent,
  PageForbiddenComponent,
  PageNotFoundComponent,
  RolesAndUsersComponent,
  ProjectsForManagerComponent,
  CreateProjectComponent,
  ViewObjectivesListForManagerComponent,
  CreateObjectiveComponent,
  ViewWorkLogsForManagerComponent,
  ProjectsForDeveloperComponent,
  ViewObjectivesListForDeveloperComponent,
  ViewWorkLogsForDeveloperComponent,
  CreateWorkLogComponent

];