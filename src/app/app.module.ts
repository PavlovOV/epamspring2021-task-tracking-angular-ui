//ng add @angular/material
// use custom style.
// npm install @auth0/angular-jwt

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { JwtModule } from "@auth0/angular-jwt";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { AuthenticationComponent } from './account/authentication/authentication.component';
// Material UI Modules
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { ConfirmDialogComponent } from './modals/confirm-dialog/confirm-dialog.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { UserSortFilterFormForAdminComponent } from './admin/roles-and-users/user-sort-filter-form/user-sort-filter-form.component';
import { PaganingComponent } from './_helpers/paganing/paganing.component';
import { ManagerGuardService } from './account/guards/manager-guard.service';
import { AdminGuardService } from './account/guards/admin-guard.service';
import { DeveloperGuardService } from './account/guards/developer-guard.service';
import { LocalStorageService } from './_helpers/local-storage.service';
import { RoleViewerService } from './account/guards/role.viewer.service';

export function tokenGetter() {
  return localStorage.getItem("jwt");
}

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,

    AuthenticationComponent,
    ConfirmDialogComponent,
    UserSortFilterFormForAdminComponent,
    PaganingComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NoopAnimationsModule,

    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:44345"],
        disallowedRoutes: []
      }
    })
  ],
  providers: [DeveloperGuardService, ManagerGuardService, AdminGuardService, LocalStorageService, RoleViewerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
