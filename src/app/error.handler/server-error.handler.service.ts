import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable()
export class serverErrorHandlerService {
  constructor(private router: Router) { }

  ParseValidationErrors(err: any, result: string[]) {
    if (err["status"] == 400) {
      if (err.error.hasOwnProperty('errors'))// get error from ModelState because from identity
        for (let e in err.error.errors)
          result.push(err.error.errors[e]);
      else {// model is not valid, do not passed because from DataAnnotations
        for (let e in err.error)
          result.push(err.error[e]);
      }
      return;
    }

    let error = "";

    if (err["status"] == 500 || err["status"] == 404)
      if (err.hasOwnProperty('statusText'))
        error = err["statusText"];

    this.router.navigate(["/page_notfound", { error: error }]);
    return;

  }

  RedirectToErrors(err: any) {
    let error: string = "";
    if (err.hasOwnProperty('statusText'))
      error = err["statusText"];

    if (err["status"] == 403) {
      this.router.navigate(["/page_forbidden", { error: error }]);
      return;
    }

    if (err["status"] == 401) {
      this.router.navigate(["/page_forbidden", { error: error }]);
      return;
    }

    if (err["status"] == 404) {
      this.router.navigate(["/page_notfound", { error: error }]);
      return;
    }

    error = error + ". to do!";
    this.router.navigate(["/page_notfound", { error: error }]);

  }
}