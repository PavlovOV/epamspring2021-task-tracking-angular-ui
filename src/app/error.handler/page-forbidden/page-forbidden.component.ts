import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-forbidden',
  templateUrl: './page-forbidden.component.html',
  
  styleUrls: ['./page-forbidden.component.css']
})
export class PageForbiddenComponent implements OnInit {
  error: string = "";
  constructor(private _route:ActivatedRoute) { }

  ngOnInit(): void {
    this.error =this._route.snapshot.paramMap.get('error')!;

  }

}
