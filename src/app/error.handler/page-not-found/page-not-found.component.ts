import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {
  error: string = " ";
  constructor(private _route:ActivatedRoute) { }

  ngOnInit(): void {
    this.error =this._route.snapshot.paramMap.get('error')!;
  }

}
